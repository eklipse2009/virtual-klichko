﻿<?php
header("Content-Type: application/json; encoding=utf-8");
 
$secret_key = 'aN1cTX4sZIWWPAGDidIO'; // Защищенный ключ приложения
 
$input = $_POST;
 
// Проверка подписи
$sig = $input['sig'];
unset($input['sig']);
ksort($input);
$str = '';
foreach ($input as $k => $v) {
  $str .= $k.'='.$v;
}              
 
if ($sig != md5($str.$secret_key)) {
  $response['error'] = array(
    'error_code' => 10,
    'error_msg' => 'Несовпадение вычисленной и переданной подписи запроса.',
    'critical' => true
  );
} else {
  // Подпись правильная
  switch ($input['notification_type']) {
    case 'get_item':
      // Получение информации о товаре
      $item = $input['item']; // наименование товара
 
if ($item == 'item1') {
        $response['response'] = array(
          'item_id' => 1, //id товара
          'title' => '10 монеток', //наименование
          'photo_url' => '', //фотография товара,если оставить поле пустым,то будет выведена иконка приложения
          'price' => 1 //цена товара в голосах
        );
      }
      break;
 
case 'get_item_test':
      // Получение информации о товаре в тестовом режиме
      $item = $input['item'];
      if ($item == 'item1') {
        $response['response'] = array(
          'item_id' => 1, //id товара
          'title' => '10 монеток(test mode)', //наименование
          'photo_url' => '', //фотография товара,если оставить поле пустым,то будет выведена иконка приложения
          'price' => 1 //цена товара в голосах
        );
      }
      break;
 
case 'order_status_change':
      // Изменение статуса заказа
      if ($input['status'] == 'chargeable') {
        $order_id = intval($input['order_id']);
        $user_id  = intval($input['user_id']);
// Код проверки товара, включая его стоимость
 
//Вконтакте может несколько раз отправлять уведомления типа Изменения статуса заказа
//(с тем же order_id) и ответ должен в точности повторять ответ для исходного уведомления.
 
$handle = fopen("file.txt","a+"); // ОТкрываем фаил,чтобы записать туда ид плтельщика и ид покупки
fwrite($handle,"\n".user_id."\n".$order_id); // Записываем
fclose($handle); // Закрываем фаил
 
 
$response['response'] = array(
          'order_id' => $order_id,
        );
      } else {
        $response['error'] = array(
          'error_code' => 100,
          'error_msg' => 'Передано непонятно что вместо chargeable.',
          'critical' => true
        );
      }
      break;
 
case 'order_status_change_test':
      // Изменение статуса заказа в тестовом режиме
      if ($input['status'] == 'chargeable') {
        $order_id = intval($input['order_id']);
        $user_id  = intval($input['user_id']);
 
$handle = fopen("file.txt","a+"); // ОТкрываем фаил,чтобы записать туда ид плтельщика и ид покупки
fwrite($handle,"\n".$user_id."\n".$order_id); // Записываем
fclose($handle); // Закрываем фаил
 
$response['response'] = array(
          'order_id' => $order_id,
        );
      } else {
        $response['error'] = array(
          'error_code' => 100,
          'error_msg' => 'Передано непонятно что вместо chargeable.',
          'critical' => true
        );
      }
      break;
  }
}
 
echo json_encode($response);
?>