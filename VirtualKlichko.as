﻿package  {
	
	import flash.display.*;
	import flash.events.KeyboardEvent;
	import flash.events.Event;
	import flash.events.MouseEvent;
	import flash.system.Security;
	//import vk.gui.SquareButton;
	//import vk.gui.InputField;
	import flash.sampler.Sample;
	import flash.text.*;
	import fl.controls.TextArea;
	import fl.controls.TextInput;
	import flash.net.*;
	import flash.utils.Timer;
	import flash.events.TimerEvent;
	
	public class VirtualKlichko extends MovieClip {
		
		private var brain:Brain;
		public var vkHandler:VkHandler;
		//public var txtUserInput:TextInput;
		private var inputFieldHelper:InputFieldHelper;
		public var userName:String;
		public var userGender:String; //m = male, f = female
		public var txtOutput:TextArea;
		//public var btnSaveScreenShot:SaveScreenShotButton;
		//public var btnPublishScreenShot:SimpleButton;
		//public var btnPublishDialog:SimpleButton;
		public var data:Object;
		private var sharedObject:SharedObject;
		private var firstPhraseTimer:Timer = new Timer(60000, 1);
		
		public function VirtualKlichko() {

			Security.loadPolicyFile("http://eklipse.ho.com.ua/crossdomain.xml");
			brain = new Brain(this);
			vkHandler = new VkHandler(this);
			
			txtOutput = this.vitalka.txtOutput;
			txtOutput.setStyle('upSkin', new Sprite());
			txtOutput.editable = false;
			sharedObject = SharedObject.getLocal('preferences');
			data = sharedObject.data;
			if (data['size']<8 || !data['size']){
				data['size']=18;
			}
			setOutputFont(data['size']);
			var textFormat:TextFormat = new TextFormat('_sans', 24/*new fntSegoePrint().fontName, 20*/, 0xBDBDBD);
			txtUserInput.setStyle('textFormat', textFormat);
			//txtUserInput.text = " "; //a SPACE or temp "foo" text, but NOT empty !
			stage.focus = txtOutput.textField;//txtUserInput.textField;
			//txtUserInput.text = "";
			//txtUserInput.textField.setSelection(0,0);
			inputFieldHelper = new InputFieldHelper(txtUserInput.textField, 'Введите сообщение');
			inputFieldHelper.init();
			//txtUserInput.textField.textColor = 0xBDBDBD;
			setChildIndex(vk_buttons_instance, numChildren-1);
			btnSay.addEventListener(MouseEvent.CLICK, this.clickHandler);
			txtUserInput.addEventListener(KeyboardEvent.KEY_DOWN, enterHandler);
			btnClear.addEventListener(MouseEvent.CLICK, clear);
			btnIncreaseFont.addEventListener(MouseEvent.CLICK, increaseFont);
			btnDecreaseFont.addEventListener(MouseEvent.CLICK, decreaseFont);
			btnUpgrade.addEventListener(MouseEvent.CLICK, showVkButtons);
			btnSaveScreenShot.addEventListener(MouseEvent.CLICK, saveScreenShot);
			btnPublishScreenShot.addEventListener(MouseEvent.CLICK, publishScreenShot);
			btnPublishDialog.addEventListener(MouseEvent.CLICK, publishDialog);
			firstPhraseTimer.addEventListener(TimerEvent.TIMER_COMPLETE, reactOnInactivity);
			firstPhraseTimer.start();
		}

		function saveScreenShot(e:Event) {
			if (!txtOutput.text) {
				say('Мы ж ещё даже не поговорили!');
				return;
			}
			showLoadingIndicator(btnSaveScreenShot);
			vkHandler.saveScreenShot();
		}
		
		function publishScreenShot(e:Event) {
			if (!txtOutput.text) {
				say('Мы ж ещё даже не поговорили!');
				return;
			}
			showLoadingIndicator(btnPublishScreenShot);
			vkHandler.publishScreenShot();
		}
		
		function publishDialog(e:Event) {
			if (!txtOutput.text) {
				say('Мы ж ещё даже не поговорили!');
				return;
			}
			showLoadingIndicator(btnPublishDialog);
			vkHandler.publishDialog();
		}
		
		function setUserAvatar(loader:Loader) {
			var f:MovieClip = vitalka.userAvatarFrame;
			loader.x = f.x;
			loader.y = f.y;
			loader.width = f.width;
			loader.height = f.height;
			vitalka.addChild(loader);
			vitalka.setChildIndex(f, vitalka.numChildren-1);
		}
		
		function clear(e:Event) {
			txtOutput.htmlText = "";
		}
			
		function setOutputFont(size:uint) {
			if (size<8) return;
			var textFormat:TextFormat = new TextFormat();
			textFormat.size = size;
			txtOutput.textField.defaultTextFormat = textFormat;
			txtOutput.textField.setTextFormat(textFormat);
			txtOutput.verticalScrollPosition = int.MAX_VALUE;
			//txtOutput.setStyle('textFormat', textFormat);
		}
		
		function setTextStyle() {
			var textFormat:TextFormat = new TextFormat();
			textFormat.size = data['size'];
			txtOutput.setStyle('textFormat', textFormat);
		}
		
		function increaseFont(e:Event) {
			data['size']+=1;
			setOutputFont(data['size']);
		}
		
		function decreaseFont(e:Event) {
			data['size']-=1;
			setOutputFont(data['size']);
		}

		function enterHandler(event: KeyboardEvent) {
			if (event.charCode == 13) {
				answerHandler.call();
			}
		}

		function clickHandler(event: MouseEvent) {
			answerHandler.call();
		}
		
		function answerHandler() {
			if (firstPhraseTimer) {
				firstPhraseTimer.stop();
				firstPhraseTimer = null;
			}
			var name:String = userName;
			if (!name) {
				name = "Вы";
			}
			if (txtUserInput.text == "Введите сообщение") {
				//txtUserInput.text = '';
				return;
			}
			setTextStyle();
			txtOutput.htmlText += "<font color = '#FF0000'>"+name+": </font>" + txtUserInput.text + 
				"<br/><font color = '#0000FF'>Виталий: </font>" + brain.getAnswer(txtUserInput.text)+"<br/>";
			txtUserInput.text = '';
			txtOutput.verticalScrollPosition = int.MAX_VALUE;
			stage.focus = txtUserInput.textField;
			var textFormat:TextFormat = new TextFormat('_sans', 24, 0x000000);
			txtUserInput.setStyle('textFormat', textFormat);
			btnSaveScreenShot.setText("Сохранить скриншот");
		}
		
		public function say(string:String) {
			setTextStyle();
			txtOutput.htmlText +="<font color = '#0000FF'>Виталий: </font>" + string + "<br/>";
		}
		
		function reactOnInactivity(e:Event) {
			say(brain.getAnswer("silence"));
			firstPhraseTimer.stop();
			firstPhraseTimer = null;
		}
		
		public function showLoadingIndicator(element:DynamicTextButton) {
			element.setTextAlpha(0);
			element.mouseEnabled = false;
			loadingIndicator.x = element.x+element.width/2-30;
			loadingIndicator.y = element.y+15;
			loadingIndicator.alpha = 1;
		}
		
		public function hideLoadingIndicator(element:DynamicTextButton) {
			loadingIndicator.alpha = 0;
			element.setTextAlpha(1);
			element.mouseEnabled = true;
		}

		function showVkButtons(event: MouseEvent): void {
			this.vk_buttons_instance.visible = true;
		}

	}
}
