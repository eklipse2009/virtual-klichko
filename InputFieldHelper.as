﻿package
{
    import flash.events.Event;
    import flash.events.EventDispatcher;
    import flash.events.FocusEvent;
    import flash.events.KeyboardEvent;
    import flash.text.TextField;
    import flash.text.TextFieldType;
    import flash.ui.Keyboard;

    public class InputFieldHelper extends EventDispatcher
    {
        private var _field:TextField;
        private var _default:String;
		private static const GRAY_COLOR = 0xBDBDBD;
		private static const BLACK_COLOR = 0x000000;
        
        public function InputFieldHelper( field:TextField, defaultText:String="" )
        {
            if(!field) throw new Error( "root::InputFieldHelper - supplied TextField must be non-null.");
            _field = field;
            _field.type = "input";
            _default = (defaultText) ? defaultText : "";
        }
        
        //start listening
        public function init()
        {
            _field.addEventListener( FocusEvent.FOCUS_IN, focusInHandler, false, 0, true );
            _field.addEventListener( FocusEvent.FOCUS_OUT, focusOutHandler, false, 0, true );
            _field.addEventListener( KeyboardEvent.KEY_UP, keyUpHandler, false, 0, true );
            
            _field.type = TextFieldType.INPUT;

			_field.textColor = GRAY_COLOR;            
			_field.text = _default;
        }
        
        //stop listening
        public function deinit():void
        {
            _field.removeEventListener( FocusEvent.FOCUS_IN, focusInHandler );
            _field.removeEventListener( FocusEvent.FOCUS_OUT, focusOutHandler );
            _field.removeEventListener( KeyboardEvent.KEY_UP, keyUpHandler );
			_field.textColor = BLACK_COLOR;
            _field.text = "";

        }
        
        public function get textField():TextField { return _field; } //the managed TextField
        public function get defaultValue():String { return _default; } //the default value of the TextField "text" value
        public function get inputValue():String { return _field.text; } //the current value of the TextField
        
/**
 * Event Listeners
 */
        private function focusInHandler(e:FocusEvent):void
        {
            if (_field.text == _default) {
				_field.text = "";
				_field.textColor = BLACK_COLOR;
			}
        }
        
        private function focusOutHandler(e:FocusEvent):void
        {
            if (_field.text.length == 0) {
				_field.text = _default;
				_field.textColor = GRAY_COLOR;
				}
            
            this.dispatchEvent( new Event( "focusSelection" ) );
        }
        
        private function keyUpHandler(e:KeyboardEvent):void
        {
            if(e.keyCode != Keyboard.ENTER) return;
            
            var str:String = _field.text;
			
            this.dispatchEvent( new Event( "keyPressSelection" ) );
        }
        
/**
 * IDisposable interface
 */
        public function dispose():void
        {
            this.deinit();
            _field = undefined;
            _default = undefined;
        }
    }
}