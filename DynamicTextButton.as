﻿package  {
	
	import flash.display.SimpleButton;
	import flash.display.MovieClip;
	import flash.display.DisplayObjectContainer;
	import flash.display.*;
	
	public class DynamicTextButton extends SimpleButton {
		
		public var index:uint;
		
				
		public function DynamicTextButton() {
			// constructor code
		}
		
		public function setText(text:String) {
			var states:Vector.<DisplayObjectContainer> = new Vector.<DisplayObjectContainer>;
			states.push(this.upState, this.overState, this.downState);
			for each (var state:DisplayObjectContainer in states) {
				TextField(state.getChildAt(1)).text = text;
				trace('text set');
			}
		}
		
		public function setTextAlpha(alpha:uint) {
			var states:Vector.<DisplayObjectContainer> = new Vector.<DisplayObjectContainer>;
			states.push(this.upState, this.overState, this.downState);
			for each (var state:DisplayObjectContainer in states) {
				DisplayObject(state.getChildAt(1)).alpha = alpha;
			}			
		}
	}
	
}
