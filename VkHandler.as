﻿package {
	import flash.display.*;
	import flash.events.*;
	import vk.APIConnection;
	import vk.events.*;
	import flash.display.MovieClip;
	import flash.display.BitmapData;
	import flash.display.Bitmap;
	import flash.display.PNGEncoderOptions;
	import flash.geom.Rectangle;
	import flash.utils.ByteArray;
	import flash.net.*;
	import flash.system.LoaderContext;	
	
	dynamic public class VkHandler extends EventDispatcher {
		
		public var isUnderDeveloperID:Boolean = false;
		private var parent:MovieClip;
		private var VK:APIConnection;
		private var voices:String = new String(); //textual representation of number of voices
		private var loader:Loader;
		private var photoUrl:String;
		
		public function VkHandler(parent:MovieClip) {
			this.parent = parent;
			var flashVars:Object = parent.stage.loaderInfo.parameters as Object;
			var api_id:Number;
			var viewer_id:Number;
			var sid:String;
			var secret:String;
			api_id = flashVars['api_id'];
			viewer_id = flashVars['viewer_id'];
			sid = flashVars['sid'];
			secret = flashVars['secret'];
			VK = new APIConnection(flashVars);
			VK.api('users.get', {user_ids: flashVars['viewer_id'], fields:'sex,photo_100,photo_200'}, onProfileLoaded, onErrorLoadingProfile);//Узнаем имя и фамилию пользователя
		}

		function onProfileLoaded(data: Object):void {
			parent.userName = data[0]['first_name']+' '+data[0]['last_name'];
			if (data[0]['sex']==1) {
				parent.userGender = 'f';
			}
			else {
				parent.userGender = 'm';
			}
			if (parent.userName=='Helga Haraldson') {
				this.isUnderDeveloperID = true;
			}
			photoUrl = data[0]['photo_200'];
			if (photoUrl==null) {
				photoUrl = data[0]['photo_100'];
			}
			trace('photoUrl = ', photoUrl);
			trace(JSON.stringify(data));
			loadUserAvatar(photoUrl);
		}

		function onErrorLoadingProfile(data:Object) {
			trace('Error loading profile:', data.error_msg);
			parent.userName = 'Helga Haraldson';
			parent.userGender  = 'm';
			loadUserAvatar('http://cs10501.vk.me/u4898077/d_4d001159.jpg');
		}
		
		function loadUserAvatar(photoUrl:String) {
			loader = new Loader();
			var req:URLRequest = new URLRequest(photoUrl);
			var context:LoaderContext = new LoaderContext();
			context.checkPolicyFile = true;
			loader.load(req, context);
			loader.contentLoaderInfo.addEventListener(Event.COMPLETE, onImageLoaded);
		}
		
		function onImageLoaded(e:Event) {
			parent.setUserAvatar(loader);
		}
	
		private function takeScreenShot():ByteArray {
			var bdata:BitmapData = new BitmapData(parent.vitalka.width, parent.vitalka.height);
			bdata.draw(parent.vitalka);
			var ba:ByteArray = new ByteArray();
			bdata.encode(new Rectangle(0, 0, 1024, 768), new PNGEncoderOptions(), ba);
			return ba;
		}
		
		private function getRequestFromByteArray(upload_url:String, ba:ByteArray) {
			var stream:ByteArray = new ByteArray();
			var boundary:String = "----------Ij5ae0ae0KM7GI3KM7";
			stream.writeUTFBytes("--"+boundary+'\r\nContent-Disposition: form-data; name="file1"; filename="Snapshot1255966065458.png"\r\nContent-Type: image/png\r\n\r\n');
			stream.writeBytes(takeScreenShot());
			stream.writeUTFBytes("\r\n--"+boundary+'--\r\n');
			var header:URLRequestHeader = new URLRequestHeader ("Content-type", "multipart/form-data; boundary="+boundary);
			var req:URLRequest = new URLRequest(upload_url);
			req.requestHeaders.push(header);
			req.method = URLRequestMethod.POST;
			req.data = stream;
			return req;
		}

//---------START OF LONG saveScreenShot chain
		public function saveScreenShot() {
			VK.api('photos.getAlbums', {}, onGotAlbums, onErrorGettingAlbums);
			function onErrorGettingAlbums(data:Object) {
				trace("cannot get albums:", data.error_msg);
				parent.hideLoadingIndicator(parent.btnSaveScreenShot);
			}
		}
		
		private function onGotAlbums(data:Object) {
			var albumFound:Boolean = false;
			for each (var album:Object in data) {
				if (album.title=='Виртуальный Кличко') {
					trace('got album!');
					albumFound = true;
					getUploadServer(album.aid);
					break;
				}
			}
			if (!albumFound) {
				VK.api('photos.createAlbum', {title:"Виртуальный Кличко", description:"Скриншоты приложения \"Виртуальный Кличко\":\n"+
					"http://vk.com/app4621676"}, onCreatedAlbum, onErrorCreatingAlbum);
				function onErrorCreatingAlbum(data:Object) {
					trace('cannot create album:', data.error_msg);
					parent.hideLoadingIndicator(parent.btnSaveScreenShot);
				}
			}
		}
		
		private function onCreatedAlbum(data:Object) {
			var aid:String = String(data['aid']);
			getUploadServer(aid);
		}
		
		private function getUploadServer(aid:String) {
			trace('album ID = ', aid);
			VK.api('photos.getUploadServer', {aid:aid}, onGotUploadServer, onErrorGettingUploadServer);
			function onErrorGettingUploadServer(data:Object) {
				trace('could not get upload server:', data.error_msg);
				parent.hideLoadingIndicator(parent.btnSaveScreenShot);
			}
		}


		private function onGotUploadServer(data:Object) {
			var req:URLRequest = getRequestFromByteArray(data['upload_url'], takeScreenShot());
			var loader:URLLoader = new URLLoader(req);
			loader.addEventListener(Event.COMPLETE, onUploadedPhoto);
			loader.addEventListener(IOErrorEvent.IO_ERROR, onErrorUploadingPhoto);
			function onErrorUploadingPhoto(e:Event) {
				trace('IOError while uploading photo:', e.target.data);
				parent.hideLoadingIndicator(parent.btnSaveScreenShot);
			}
		}
		
		private function onUploadedPhoto(e:Event) {
			var json_data = JSON.parse(e.target.data);
			var photos_list:String = json_data['photos_list'];
			var aid:String = String(json_data['aid']);
			var server:String = json_data['server'];
			var hash:String = json_data['hash'];
			VK.api("photos.save", {aid:aid, server:server, photos_list:photos_list, hash:hash,
				caption:"Виртуальный Кличко! http://vk.com/app4621676_4898077"}, onSavedPhoto, onErrorSavingPhoto);
			function onErrorSavingPhoto(data:Object) {
				trace('Error saving photo:', data.error_msg);
				parent.hideLoadingIndicator(parent.btnSaveScreenShot);
			}
		}
		
		private function onSavedPhoto(data:Object) {
			parent.hideLoadingIndicator(parent.btnSaveScreenShot);
			var url:String = 'http://vk.com/'+data[0]['id'];
			this.photoUrl = url;
			parent.btnSaveScreenShot.removeEventListener(MouseEvent.CLICK, parent.vkHandler.saveScreenShot);
			parent.btnSaveScreenShot.addEventListener(MouseEvent.CLICK, popupPhotoUrl);
			parent.btnSaveScreenShot.setText('Перейти в альбом');
			//parent.btnSaveScreenShot.label = "Посмотреть в альбоме";
		}
			
		private function popupPhotoUrl(e:MouseEvent) {
			var req:URLRequest = new URLRequest(photoUrl);
			navigateToURL(req);
			parent.btnSaveScreenShot.removeEventListener(MouseEvent.CLICK, popupPhotoUrl);
			parent.btnSaveScreenShot.addEventListener(MouseEvent.CLICK, parent.vkHandler.saveScreenShot);
		}
		
//----------END OF saveScreenShot chain
		
		public function publishScreenShot() {
			VK.api('photos.getWallUploadServer', {}, onGotWallUploadServer, onErrorGettingWallUploadServer);
			function onErrorGettingWallUploadServer(data:Object) {
				trace('Error getting wall upload server:', data.error_msg);
				parent.hideLoadingIndicator(parent.btnPublishScreenShot);
			}
		}
		
		private function onGotWallUploadServer(data:Object) {
			var req:URLRequest = getRequestFromByteArray(data['upload_url'], takeScreenShot());
			var loader:URLLoader = new URLLoader(req);
			loader.addEventListener(Event.COMPLETE, onUploadedWallPhoto);
			loader.addEventListener(IOErrorEvent.IO_ERROR, onErrorUploadingWallPhoto);
			function onErrorUploadingWallPhoto(e:Event) {
				trace('IOError while uploading photo:', e.target.data);
				parent.hideLoadingIndicator(parent.btnPublishScreenShot);
			}
		}
		
		private function onUploadedWallPhoto(e:Event) {
			var json_data = JSON.parse(e.target.data);
			var photo:String = json_data['photo'];
			var server:String = json_data['server'];
			var hash:String = json_data['hash'];
			VK.api("photos.saveWallPhoto", {server:server, photo:photo, hash:hash}, onSavedWallPhoto, onErrorSavingWallPhoto);
			function onErrorSavingWallPhoto(data:Object) {
				trace('Error saving photo:', data.error_msg);
				parent.hideLoadingIndicator(parent.btnPublishScreenShot);
			}
		}
		
		private function onSavedWallPhoto(data:Object) {
			trace(JSON.stringify(data));
			var photo_id:String = data[0]['id'];
			VK.api('wall.post', {message: "Мой диалог с Виртуальным Кличко:\nhttp://vk.com/app4621676", 
				attachments:photo_id+",http://vk.com/app4621676"},
				onPostWallPhoto, onErrorPostingWallPhoto);
			function onErrorPostingWallPhoto(data:Object) {
				trace("Error posting wall photo:", data.error_msg);
				parent.hideLoadingIndicator(parent.btnPublishScreenShot);
			}
		}
		
		private function onPostWallPhoto(data:Object) {
			trace('posted wall photo successfully:', JSON.stringify(data));
			parent.hideLoadingIndicator(parent.btnPublishScreenShot);
		}
		
		public function publishDialog() {
			var dialog:String = parent.txtOutput.text;
			while (dialog.length>1000 || dialog.substr(0, parent.userName.length)!=parent.userName) {
				dialog = dialog.substr(1, int.MAX_VALUE);
				if (!dialog) {
					dialog = "Виртуальный Кличко!\n Пообщайтесь с виртуальной версией знаменитого спортсмена и политика!\nhttp://vk.com/app4621676";
					break;
				}		
			}
			dialog = dialog.split("\r").join("\n");
			VK.api('wall.post', {message:dialog,
				attachments:"photo4898077_344713405,http://vk.com/app4621676"}, wallPostSuccess, wallPostFail);
		}
		
		public function shareWithFriends(e:Event) {
			VK.api('wall.post', {message:"Виртуальный Кличко!\n Пообщайтесь с виртуальной версией знаменитого спортсмена и политика!\nhttp://vk.com/app4621676",
				attachments:"photo4898077_344713405,http://vk.com/app4621676"}, wallPostSuccess, wallPostFail);
		}
		
		private function wallPostSuccess(data: Object): void {
			trace("Success wall.post post_id: "+data.post_id.toString()+"\n");
			parent.hideLoadingIndicator(parent.btnPublishDialog);
		}

		private function wallPostFail(data: Object): void {
			trace("Fail wall.post error_msg: "+data.error_msg+"\n");
			parent.hideLoadingIndicator(parent.btnPublishDialog);
		}
		
		public function process_payment(n:int):void {
			voices = n.toString()+' голосов';
			try {
				VK.addEventListener('onOrderSuccess', payment_done); //если все прошло удачно
				VK.addEventListener('onOrderCancel', payment_cancelled); //если пользователь отменил передачу
				VK.addEventListener('onOrderFail', payment_failed); //если произошла ошибка
				VK.callMethod('showOrderBox', { type:'votes',votes:n});
			}
			catch(error:Error) {
				trace("could not complete payment: ", error);
				parent.vk_buttons_instance.visible = false;
				parent.txtOutput.htmlText+="<p>Ошибка:"+error.toString()+"</p>";
			}
		}
		
		
		function payment_done(e:Event):void {
			parent.vk_buttons_instance.visible = false;
			parent.txtOutput.htmlText += "<font color = '#0000FF'>"+"Виталий: </font>Спасибо! Вы перевели "+this.voices+'.';
			parent.txtOutput.verticalScrollPosition = int.MAX_VALUE;	
		}
		
		function payment_cancelled(e:Event):void {
			parent.vk_buttons_instance.visible = false;
			parent.txtOutput.htmlText += "<font color = '#0000FF'>"+
			"Виталий: </font>А жаль...";
			parent.txtOutput.verticalScrollPosition = int.MAX_VALUE;	
		}
		
		function payment_failed(e:Event):void {
			parent.vk_buttons_instance.visible = false;
			parent.txtOutput.htmlText += "<font color = '#0000FF'>"+
			"Виталий: </font>Что-то не вышло.</p><br /><p>"+e.target.toString()+"</p>";
			parent.txtOutput.verticalScrollPosition = int.MAX_VALUE;	
		}
	}
}