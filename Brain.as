﻿package {
	
    import flash.net.URLRequest;
    import flash.net.URLLoader;
    import flash.events.Event;
	import flash.net.URLRequestMethod;
	import flash.net.navigateToURL;
	import flash.display.MovieClip;
	
	dynamic public class Brain {
		
		private var dictionary:Vector.<Array>;
		private var roulette:Array;
		private var smart_roulette:Vector.<Array>;
		private var phrases:Array;
		private var usedTwists:Array;
		public var parent:MovieClip;
		private var roulette_or_phrases:Boolean;
		
		public function Brain(parent:MovieClip):void {
			this.parent = parent;
			loadDictionary();
			loadRoulette();
			loadSmartRoulette();
			loadPhrases();
		}

		function getRandomElementOf(array:Array):String {
			var idx:int=Math.floor(Math.random() * array.length);
			return array[idx];
		}

		function loadDictionary() {
			var data:dict_text = new dict_text();
			var lines:Array = data.toString().split('\r\n');
			dictionary = new Vector.<Array>();
			for (var line:String in lines) {
				var lineA:Array = lines[line].split('/');
				var key:Array = lineA[0].split(' ');
				for (var subkey:* in key) {
					key[subkey] = key[subkey].replace(/_/g, " ");
				}
				var qvalue:Array = lineA[1].split('*');
				var newitem:Array = new Array(key, qvalue);
				dictionary[dictionary.length]=newitem;
			}			
		}
		
		function loadRoulette() {
			var data:roulette_text = new roulette_text();
			roulette = data.toString().split('\r\n');					
		}
		
		function loadSmartRoulette() {
			var data:smart_roulette_text = new smart_roulette_text();
			var lines:Array = data.toString().split('\r\nSECTION_SPLIT\r\n');
			smart_roulette = new Vector.<Array>;
			for each (var line:String in lines) {
				smart_roulette.push(line.split('\r\n')); 
			}
		}
		
		function loadPhrases() {
			var data:phrases_text = new phrases_text();
			phrases = data.toString().split('\r\n');
		}

		public function getTwist():String {
			var randint:uint = uint(Math.random()*roulette.length);
			var twist:String = roulette[randint];
			if (usedTwists.indexOf(twist)>=0) {
				return this.getTwist.call();
			}
			else {
				usedTwists.push(twist);
				return twist;
			}
		}
		
		private function getFromDict(userInput:String):String {
			
			var answer:String = new String;
			for (var key:* in dictionary) {
				for (var subkey:* in dictionary[key][0]) {
					var word:String = dictionary[key][0][subkey];
					var isValidKey:Boolean = false;
					if (word.indexOf('*')>=0) {
						var unmasked:String = word.replace('*','');						
						if (userInput.indexOf(unmasked)==0) {
							isValidKey = true;
						}
					}
					else if (userInput.indexOf(word)>=0) {
						isValidKey = true;
					}
					else if (word=='zeropunctuation' && userInput.length>1) {
						var lastChar = userInput.charAt(userInput.length-1);
						 if (lastChar!='.' && lastChar!='?' && lastChar!='!') {
							isValidKey = true;
						}
					}
					if (isValidKey) {
						trace('pattern = ', word);
						answer = getRandomElementOf(dictionary[key][1]);
						while (answer.indexOf("rand")>=0) {
							answer = answer.replace("rand", this.getTwist.call());
						}
						var userPart:String;						
						if (word.indexOf("*")>=0) {
							var strstart:uint = word.indexOf('*');
							userPart = userInput.substr(strstart+1, userInput.length-strstart-1);
							userPart = userPart.replace(/[.?!]/g, '');
						}
						else {
							userPart = userInput;
						}
						dictionary[key][1].splice(dictionary[key][1].indexOf(answer), 1);
						while (answer.indexOf("[]")>=0) {
							answer = answer.replace("[]", userPart);
						}
						if (parent.userGender == 'm') {
							answer = answer.replace(/m:| f:+[\S]*/g, '');
						}
						else {
							answer = answer.replace(/ f:|m:+[\S]*/g, '');
						}						
						trace('pattern answers left = ', dictionary[key][1].length);
						if (dictionary[key][1].length==0) {
							dictionary.splice(key, 1);
						}
						return answer.substr(0,1).toUpperCase()+answer.substr(1, answer.length-1)
					}
				}
			}
			return '';
		}
		
		private function getRoulette(userInput:String):String {
			var answer:String = new String;
			roulette_or_phrases = !roulette_or_phrases;
			if (roulette_or_phrases==false && phrases.length) { //use phrases
				answer = getRandomElementOf(phrases);
				phrases.splice(phrases.indexOf(answer), 1);
			}
			else {
				for each (var part:Array in smart_roulette) { //use roulette
					answer+=getRandomElementOf(part);
				}
			}
			return answer;
			//Below - older implementation, completely randomized.
			/*var twistsneeded:uint = uint(Math.random()*3)+2;
			for (var i:Number = 1;i<=twistsneeded;i++) {
				answer = answer+this.getTwist.call()+', '
			}
			return answer.substr(0,1).toUpperCase()+answer.substr(1,answer.length-3)+'.';*/
		}

		public function getAnswer(userInput:String):String {
			usedTwists = new Array;
			userInput = userInput.toLowerCase();
			var answer:String = new String;
			saveInput(userInput);
			answer = getFromDict(userInput);
			if (!answer) {
				trace('key not found:', userInput);
				answer = getRoulette(userInput);
			}
			return answer;
		}

		public function saveInput(userInput:String):void {
			if (userInput=='') return;
			if (parent.userName) {
				userInput = parent.userName+': '+userInput;
			}
			var urlreq:URLRequest = new URLRequest("http://eklipse.ho.com.ua/klichko/klichko.php?phrase="+userInput);
			urlreq.method = URLRequestMethod.POST;
			var loader:URLLoader = new URLLoader(urlreq);
			//loader.load(urlreq);
		}
}


}