﻿package  {
	
	import flash.display.SimpleButton;
	import flash.display.MovieClip;
	import flash.display.DisplayObjectContainer;
	
	public class DynamicButton extends SimpleButton {
		
		public var index:uint;
				
		public function DynamicButton() {
			// constructor code
		}
		
		public function setText(text:String) {
			var states:Vector.<DisplayObjectContainer> = new Vector.<DisplayObjectContainer>;
			states.push(this.upState, this.overState, this.downState);
			for each (var state:DisplayObjectContainer in states) {
				TextField(state.getChildAt(1)).text = text;
			}
		}
	}
	
}
